from __set_path import set_path
set_path()
import testbase
import unittest
import neuralnetwork.neuralnetwork as nn
import numpy as np

class TestCase (testbase.TestCase):
    def test_neuralnetwork(self):
        connection_rate = 1
        learning_rate = 0.7
        learning_momentum = 0.01
        n_input = 2
        n_hidden = 4
        n_output = 1
        desired_error = 0.0001
        epoch = 1000
        show = 10
        hidden_activation = nn.SIGMOID_SYMMETRIC
        output_activation = nn.SIGMOID_SYMMETRIC
        training_algorithm = nn.TRAIN_RPROP 
        network = [2,4,1]
        
        X = np.array([[0,0],[1,0],[0,1],[1,1]])
        y = np.array([[0],[1],[1],[0]])
        
        n = nn.NeuralNetwork(network=network, 
                          connection_rate=connection_rate,
                          learning_rate=learning_rate,
                          learning_momentum=learning_momentum,
                          desired_error=desired_error,
                          epoch=epoch,
                          hidden_activation=hidden_activation,
                          output_activation=output_activation,
                          training_algorithm=training_algorithm,
                          show=show)
        
        n.fit(X,y)
        yhat = n.predict(X)
        score = n.score(y, yhat)
        self.assertEqual(0.0, n.score(y, yhat))
                         
if __name__=='__main__':
    unittest.main()