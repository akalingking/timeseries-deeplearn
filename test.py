from __future__ import division
'''
Copyright (c) 2017 sequenceresearch.com - All Rights Reserved
Unauthorized copying of this file, via any medium is strictly prohibited
PROPRIETARY AND CONFIDENTIAL
@author akalingking@sequenceresearch.com
'''
'''
History
[1]

Reference
[1] Usage of library
    https://www.codeproject.com/articles/13091/artificial-neural-networks-made-easy-with-the-fann
    https://genbattle.bitbucket.io/blog/2016/03/19/Simple-Artificial-Neural-Networks-with-FANN-and-C/
    http://leenissen.dk/fann/html/files/fann_cpp-h.html#FANN.training_algorithm_enum
'''
# import os  
import numpy as np  
import pandas as pd
# import matplotlib.pyplot as plt
# from matplotlib.backends.backend_pdf import PdfPages  
import statsmodels.api as sm  
# # from statsmodels.tsa.stattools import adfuller, acf
# import multiprocessing as mp
# from scipy import stats
from datetime import datetime
# # import statsmodels
# from statsmodels.stats import diagnostic
# from pandas.tseries.offsets import BDay # for generating business days
# from statsmodels.tsa import stattools # manual acf and pacf
from pandas.tseries.offsets import BDay
import neuralnetwork.neuralnetwork as nn 
from neuralnetwork import plot, datasource

FILEPATH = '/home/akalingking/dataset/oanda/EURUSD1440.csv'
RESULTPATH = '/home/akalingking/src/python/v2/fx-deeplearn/results/'
FORECASTSTEPS = 20
OUTLIERFACTOR = 3

def partition_data(ts, stardate, enddate, testenddate=None):
    pass

def undiff(diff, startIndex, startValue):
    '''
    @param    diff: series to recover
    @param    startIndex: initial index
    @param    startValue: initial value
    @retval   undifference ts padded with the startvalue, len(diff)+1
    '''
    ret = pd.Series(data=startValue, index=[startIndex])
    ret = ret.append(diff)
    for i in xrange(0, len(diff)):
        ret[i+1] = ret[i] + diff[i]
    return ret

def main():
    '''
    [1] Construct ts from csv file
    '''
    close = datasource.read_ts_from_csv(FILEPATH, col='Close')
    assert (close is not None)

    # detect outliers
    stdev =  np.std(close,ddof=0)
    mean = np.mean(close)
    temp = close[np.abs(close - mean) > (stdev*OUTLIERFACTOR)]
    if len(temp) > 0:
        print "Outliers detected"
        close[np.abs(close - mean) > (stdev*2)] = mean+stdev*OUTLIERFACTOR
        
    '''
    construct training and test set
    '''
#     start = datetime.strptime("2011-01-01", "%Y-%m-%d")
#     end = datetime.strptime("2015-12-31", "%Y-%m-%d")
    train_startdate = datetime.strptime("2016-11-01", "%Y-%m-%d")
    train_enddate = datetime.strptime("2016-11-30", "%Y-%m-%d")
    test_startdate = (train_enddate + BDay(1)).to_pydatetime()
    test_enddate = close.index[-1]
    
    train = close[train_startdate:train_enddate]
    test = close[test_startdate:test_enddate]
#     dataset['train'] = train
#     dataset['test'] = test
    
    # neural network parameters
    connection_rate = 1
    learning_rate = 0.7
    learning_momentum = 0.01
    n_input = 2
    n_hidden = 3
    n_output = 1
    desired_error = 0.0001
    epoch = 1000
    show = 10
    hidden_activation = nn.SIGMOID_SYMMETRIC
#     output_activation = nn.SIGMOID_SYMMETRIC
    output_activation = nn.LINEAR
#     training_algorithm = nn.TRAIN_RPROP 
    training_algorithm = nn.TRAIN_INCREMENTAL
    network = [n_input, n_hidden, n_output]
    
    n = nn.NeuralNetwork(network=network, 
                          connection_rate=connection_rate,
                          learning_rate=learning_rate,
                          learning_momentum=learning_momentum,
                          desired_error=desired_error,
                          epoch=epoch,
                          hidden_activation=hidden_activation,
                          output_activation=output_activation,
                          training_algorithm=training_algorithm,
                          show=show)
        
    diff = (train - train.shift(1)).dropna()
    diff2 = ( diff - diff.shift(1) ).dropna()
    lag1 = ( diff.shift(1) ).dropna()
    
#     df  = pd.DataFrame(data={'diff2':diff2.values, 'diff':diff.values[1:]}, index=diff2.index)
#     X = df.values[:, :]
#     y = train.values[2:]
    
    df  = pd.DataFrame(data={'diff2':lag1.values, 'train':train.values[2:]}, index=diff2.index)
    X = df.values[:, :]
    y = diff.values[1:]
    print 'X\n', X
    print 'Y\n', np.vstack(y)
#     return
# 
#     X = np.array([[0.00001],[0.000012],[0.00001212],[0.00012]])
#     y = np.array([[0],[1],[1],[0]])
     
#     print X
#     print y

#     print ('start fit')
    n.fit(X,y)
#     print ('end fit')
    
#     print ('start predict')
    yhat = n.predict_proba(X)[:,1]
    # conver the sequence to row vector
#     y = np.vstack(y)
    print "y", y
    print "yhat", yhat
    
    print ('end predict')
    
    score = n.score(np.vstack(y), yhat)
    print ('normalized rmse: %0.10f' % score)
    
    ts = pd.Series(data=yhat, index=train.index[2:])
#     print ts
#     print train[2:]
    y = train[2:]
    yhat = undiff(ts, train.index[1], train[1])[1:]
#     print 'y\n', y
#     print 'yhat\n', yhat[1:]
    rmse = sm.tools.eval_measures.rmse(y.values, yhat.values)
    print 'actual rmse %0.10f' % rmse
    df = pd.DataFrame(data={'y':y.values, 'yhat':yhat.values}, index=y.index)
    plot.plot_ts(df, 'EURDLR_Close', resultpath=RESULTPATH)
    
    
if __name__=='__main__':
    main()
    
   