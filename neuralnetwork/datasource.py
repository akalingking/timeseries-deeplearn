'''
Copyright (c) 2017 sequenceresearch.com - All Rights Reserved
Unauthorized copying of this file, via any medium is strictly prohibited
PROPRIETARY AND CONFIDENTIAL
@author akalingking@sequenceresearch.com
'''
import pandas as pd

def read_ts_from_csv(filepath, col='Close'):
    '''
    @brief constructs dataframe from csv file then returns selected series
    @param filepath
    @param col
    @rtype pandas.Series
    '''
    # static
    read_ts_from_csv.headers_ = ['Datetime','Open','High','Low','Close', 'Volume']
    ret = None
    raw_data = None
    
    print('readfile %s' % filepath)
    
    #@note oanda dataset uses first 2 columns for data and time separately
    parse_dates_ = [[0,1]]
    try:
        dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
        raw_data = pd.read_csv(filepath, 
                     delimiter=',', 
                     header=None, 
                     parse_dates= parse_dates_,
                     date_parser=dateparser)
        
        raw_data.columns = read_ts_from_csv.headers_
        raw_data.set_index(['Datetime'], drop=True, inplace=True)
        print raw_data.info()
        print raw_data.head(5)
    except:
        print ("Exception reading source file")
    
    if raw_data is not None:
        try:
            ret = pd.Series(data=raw_data[col].values, index=raw_data.index)
        except Exception as e:
            print ('Exception: %s' % e)
               
    return ret