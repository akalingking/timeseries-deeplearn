'''
Copyright (c) 2017 sequenceresearch.com - All Rights Reserved
Unauthorized copying of this file, via any medium is strictly prohibited
PROPRIETARY AND CONFIDENTIAL
@author akalingking@sequenceresearch.com
'''

from fann2 import libfann
from sklearn.base import BaseEstimator
import numpy as np

# Forward symbols from the fann library
LINEAR                      = libfann.LINEAR
LINEAR_PIECE                = libfann.LINEAR_PIECE
LINEAR_PIECE_SYMMETRIC      = libfann.LINEAR_PIECE_SYMMETRIC
ERRORFUNC_LINEAR            = libfann.ERRORFUNC_LINEAR
ELLIOT                      = libfann.ELLIOT
GAUSSIAN                    = libfann.GAUSSIAN
SIGMOID                     = libfann.SIGMOID                       # 3 0,1
SIGMOID_STEPWISE            = libfann.SIGMOID_STEPWISE              # 4 0,1
SIGMOID_SYMMETRIC           = libfann.SIGMOID_SYMMETRIC             # 5 tanh -1,1
SIGMOID_SYMMETRIC_STEPWISE  = libfann.SIGMOID_SYMMETRIC_STEPWISE    # 6 -1,1 faster than 5 but less precise
TRAIN_INCREMENTAL           = libfann.TRAIN_INCREMENTAL             # 0
TRAIN_BATCH                 = libfann.TRAIN_BATCH
TRAIN_RPROP                 = libfann.TRAIN_RPROP
TRAIN_QUICKPROP             = libfann.TRAIN_QUICKPROP
TRAIN_SARPROP               = libfann.TRAIN_SARPROP
      
class NeuralNetwork(BaseEstimator):
    DEFAULT_NETWORK = [2,4,1]    
    def __init__(self, 
                 network=DEFAULT_NETWORK, 
                 connection_rate=1,
                 learning_rate=0.6,
                 learning_momentum=0.01,
                 desired_error=0.0001,
                 epoch=100,
                 initial_weight=1,
                 hidden_activation=SIGMOID,
                 output_activation=SIGMOID,
                 training_algorithm = TRAIN_INCREMENTAL, 
                 show=100):
        self.network = network
        self.connection_rate = connection_rate
        self.learning_rate = learning_rate
        self.learning_momentum = learning_momentum
        self.desired_error = desired_error
        self.initial_weight = initial_weight
        self.epoch = epoch
        self.show = show
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation
        self.training_algorithm = training_algorithm
        self.ann = None 
        
        
    def _create_network(self):
        """ create_sparse_array Creates a standard back propagation neural network """
        self.ann = libfann.neural_net()
        
        """ Setup the network """
        self.ann.create_sparse_array(self.connection_rate, self.network)
        self.ann.set_learning_rate(self.learning_rate)
        self.ann.set_learning_momentum(self.learning_momentum)
        self.ann.randomize_weights(-self.initial_weight,self.initial_weight)
        self.ann.set_training_algorithm(self.training_algorithm)
        
        """ set activation function """
        self.ann.set_activation_function_hidden(self.hidden_activation)
        self.ann.set_activation_function_output(self.output_activation)
       
        """ This option is only used in cascading network """
        #ann.set_training_algorithm(libfann.TRAIN_INCREMENTAL)
        
    
    def fit(self, X, y):
        self._create_network()
        x_train = libfann.training_data()
        if len(y.shape) == 1 and y.shape[0] > 0:
            """ fann requires a row vector"""
            y = y[:, np.newaxis]
        x_train.set_train_data(X, y)
        self.ann.train_on_data(x_train, self.epoch, self.show, self.desired_error)
        #print "Training MSE error on test data: %f" % self.ann.get_MSE()
    
    
    def predict_proba(self, X):
        self.ann.reset_MSE()
        y = np.array([ self.ann.run(x) for i,x in enumerate(X) ])
        return np.c_[y, y]
    
    
    def predict(self, X):
        result = self.predict_proba(X)[:,1]
        # use either 1 or 0
        y_pred = np.array([(1 if x > 0.5 else 0) for x in result])
        return y_pred
    
    
    def score(self, X, y, sample_weight=None):
        from sklearn import metrics
        return np.sqrt(metrics.mean_squared_error(y, self.predict(X)))
        

