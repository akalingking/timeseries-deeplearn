'''
Copyright (c) 2017 sequenceresearch.com - All Rights Reserved
Unauthorized copying of this file, via any medium is strictly prohibited
PROPRIETARY AND CONFIDENTIAL
@author akalingking@sequenceresearch.com
'''
from __future__ import division 
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages  
import statsmodels.api as sm  
# from statsmodels.tsa.stattools import adfuller, acf
import multiprocessing as mp
# from scipy import stats
# from datetime import datetime
# import statsmodels
# from statsmodels.stats import diagnostic
# from pandas.tseries.offsets import BDay # for generating business days
from statsmodels.tsa import stattools # manual acf and pacf

PLTFONTSIZE = 8
RCPARAMS = {'axes.titlesize':PLTFONTSIZE,
             'axes.labelsize':PLTFONTSIZE,
             'legend.fontsize':PLTFONTSIZE,
             'xtick.labelsize':PLTFONTSIZE,
             'ytick.labelsize':PLTFONTSIZE,}

def __get_frequency(len):
    if len > 1000:
        return 252
    elif (len <= 1000 and len >= 100):
        return 30
    if (len > 30 and len < 100):
        return 5
        
def __plot_ts(data, title, resultpath, show):
    '''
    @param data datafram instance
    '''
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, figsize=(12,10))
    plt.rcParams.update(RCPARAMS)
    data.plot.line(ax=axes)
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=5)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=5)
     
    # enable grids
    [a.grid(True, which='both') for a in fig.axes]
    for a in fig.axes:
        a.grid(True, which='both')
#     plt.grid()
#     for a in fig.axes:
#         a.xaxis.grid(True)
#         a.yaxis.grid(True)
    
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    
    # save to pdf
    pp = PdfPages(resultpath + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    
    # show to desktop, call is blocking
    if show:
        plt.show()
        
def plot_ts(data, title=None, resultpath="./", show=False):
    mp.Process(target=__plot_ts, args=(data, title, resultpath, show)).start()

def __plot_stationarity(data, name, window, resultpath, show):
    '''
    @param data    series
    @param name    name of series
    @param window  lag  
    '''
    
    # Check if we have sufficient datapoint for the window
    if (len(data) < window or window is None):
        window = __get_frequency(len(data))
    '''
    [1] Variables for stationarity plots
    '''
    rollingMean = pd.Series.rolling(data, window=window).mean()
    rollingStd = pd.Series.rolling(data, window=window).std()
    s = pd.DataFrame(data=data, index=data.index, columns=['Original'])
    s['Rolling-Mean'] = rollingMean
    s['Rolling-Stdev'] = rollingStd
    '''
    [2] Decompose timeseries
    '''
    composition = sm.tsa.seasonal_decompose(data, model='additive', freq=window)
    '''
    [3] Show plots
    '''    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    
    #{@ Plot
    s.plot(ax=axes[0], title='Data', legend='best')
    composition.trend.plot(ax=axes[1], title='Trend')
    composition.seasonal.plot(ax=axes[2], title='Seasonal')
    composition.resid.plot(ax=axes[3], title='Residual')
    #}@plot
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(name, y=1.0)
    # save to pdf
    pp = PdfPages(resultpath +'stationarity_' + name + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
    if show:
        plt.show()
        
def plot_stationarity(data, name, window=None, resultpath="./", show=False):
    mp.Process(target=__plot_stationarity, args=(data, name, window, resultpath, show)).start()
    
def __plot_autocorrelation(data, name, window, resultpath, show):
    '''
    Plots autocorrelation
    '''
    def get_ymax(x):
        sorted = np.sort(x)
        ret= max(sorted[-2], np.abs(sorted[0])) #acf and pacf's [-1] is always 1
        ret = ret * 1.5
        return ret
    
    # Check if we have sufficient datapoint for the window
    if (len(data) < window or window is None):
        window = __get_frequency(len(data))
    
    # Prepare figure    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    # plot the original data
    data.plot.line(ax=axes[0], title=name)
    
    #{ plot
    acf_ = stattools.acf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_acf(data.values.squeeze(), lags=window, ax=axes[1])
    ymax = get_ymax(acf_)
    axes[1].set_ylim(-ymax, ymax)
    
    pacf_ = stattools.pacf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_pacf(data, lags=window, ax=axes[2])
    ymax = get_ymax(pacf_)
    axes[2].set_ylim(-ymax, ymax)
    
    # http://statsmodels.sourceforge.net/devel/generated/statsmodels.graphics.gofplots.qqplot.html
    sm.qqplot(data=data, ax=axes[3], line='r')
    #} plot
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle('Correlation: ' + name, y=1.0)
    
    # save to pdf
    pp = PdfPages(resultpath + 'correlation_' + name + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    
    #show to desktop, call is blocking
    if show:
        plt.show()
        
def plot_autocorrelation(data, name, window=None, resultpath="./", show=False):
    mp.Process(target=__plot_autocorrelation, args=(data, name, window, resultpath, show)).start()
    