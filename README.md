# Timeseries-DeepLearn #

### Summary ###

* Neural Network for predicting currency prices
* Version 1.0

### Requirements ###

* Python 2.7
* Numpy
* Scipy
* Statsmodels
* Sklearn
